open Mjs
open Vue_component

type vue_component = any

class type mixins = object
  method reactiveData : any readonly_prop
  method reactiveProp : any readonly_prop
end

class type vue_charts = object
  method _Bar : vue_component readonly_prop
  method _HorizontalBar : vue_component readonly_prop
  method _Doughnut : vue_component readonly_prop
  method _Line : vue_component readonly_prop
  method _Pie : vue_component readonly_prop
  method _PolarArea : vue_component readonly_prop
  method _Radar : vue_component readonly_prop
  method _Bubble : vue_component readonly_prop
  method _Scatter : vue_component readonly_prop
  method mixins : mixins t readonly_prop
  method generateChart : Unsafe.any readonly_prop
end

let vue_charts : vue_charts t optdef = Unsafe.pure_js_expr "VueChartJs"

let make typ id = make
    ~extends:typ
    ~props:(PrsArray ["data"; "options"])
    ~lifecycle:[
      "mounted", (fun this ->
          let this = Unsafe.coerce this in
          this##renderChart this##.data this##.options)]
    id

let make_line () =
  match Optdef.to_option vue_charts with
  | Some v -> make v##._Line "line-chart"
  | None -> log_str "VueChartJs not found"; Unsafe.obj [||]

let make_bar () =
  match Optdef.to_option vue_charts with
  | Some v -> make v##._Bar "bar-chart"
  | None -> log_str "VueChartJs not found"; Unsafe.obj [||]

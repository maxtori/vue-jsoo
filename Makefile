all: build stubs

build:
	@dune build src

stubs:
	@dune build js

test: all
	@dune build test --profile release
	@cp -f _build/default/test/test_vue.bc.js test/test-vue.js
	@cp -f _build/default/test/test_router.bc.js test/test-router.js
	@cp -f _build/default/test/test_chart.bc.js test/test-chart.js
	@cp -f _build/default/test/test_vuex.bc.js test/test-vuex.js
	@cp -f _build/default/test/test_vuex2.bc.js test/test-vuex2.js
	@cp -f _build/default/test/test_project.bc.js test/test-project.js

clean:
	@dune clean

install:
	@dune install

doc:
	@dune build @doc
	@cp -rf _build/default/_doc/_html/* docs

compiler-deps:
	@sudo npm i -g @vercel/ncc

compiler:
	@npm i --prefix js --no-audit --no-fund
	@js/node_modules/@vercel/ncc/dist/ncc/cli.js build js/vue-jsoo-compiler.js -m -q
	@mv dist/index.js js/vue-jsoo-compiler.bundle.js
	@rm -rf dist
